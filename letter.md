### Use and support of open-source interconnected social media platforms by the Commission

The Commission informs the public via various commercial, closed-source and centralized non-EU based social media and content hosting platforms which are accessible only to users that agree to legally and ethically controversial practices. It does not so far inform citizens via federated (ie. interconnected) platforms that support open-source social networking protocols such as ActivityPub (eg. Mastodon, Pixelfed, Diaspora, Friendica, Peertube). These federated platforms (also known as the Fediverse) currently have 4 million users and counting, which is a non-negligeable number of people to be potentially reached out.

1. Which EU policy objectives (such as EU technological sovereignty, interoperability and user choice) is the use of open-source interconnected social and publishing platforms by public authorities consistent with?

2. Is the Commission looking into disseminating its messages also via open-source interconnected social networking and publishing platforms, at least by means of cross-posting / mirroring the content produced for existing channels?

3. Has the Commission looked into setting up their own instances of such services?
